# Package

version       = "0.1.0"
author        = "Xabier Bello"
description   = "Nim wrapper for edlib"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6.10"
