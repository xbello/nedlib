## Wraps `edlib <https://github.com/Martinsos/edlib>`_ C library, slightly
## modified and added some convenience procedures.
##
## The original edlib docs should match this document, with the following
## changes:
##
## - Removed every "EDLIB" and "Edlib" in namings.
##
## - Enum/Object names simplified. E.g. original `EdlibAlignTask` here is
##   `Task`, original `EDLIB_TASK_DISTANCE` here is `Task.DISTANCE`.
##
##
## Examples
## ========
##
## Find the edit distance between two string, mode NW, no path, no locations:
##
## .. code-block:: Nim
##
##    import nedlib
##
##    let cfg = defaultAlignConfig()
##    echo align("SUCCESS", "ACCESS", cfg)
##    freeAlignResult(result)
##
## Output:
##
## .. code::
##
##    (status: 0, editDistance: 2, endLocations: ..., startLocations: ...,
##    numLocations: 1, alignment: nil, alignmentLength: 0, alphabetLength: 5)
##
## You are probably interested in `editDistance` value.
##
## To find the alignment path between two strings:
##
## .. code-block:: Nim
##
##    import nedlib
##
##    let cfg = newAlignConfig(-1, Mode.HW, Task.PATH, nil, 0)
##
##    let result = align("SUCCESS", "ACCESS", cfg)
##
##    echo "RESULT: ", result
##    echo "CIGAR: ", alignmentToCigar(result)
##    freeAlignResult(result)
##
## Output:
##
## .. code::
##
##     RESULT: (status: 0, editDistance: 2, endLocations: ...,
##     startLocations: ..., numLocations: 1, alignment: , alignmentLength: 7,
##     alphabetLength: 5)
##     CIGAR: 1M1I5M
##
## CIGAR is a Cigar string that can be translated as "1 Match, 1 Insertion, 5
## Matches" and represent the alignment between the two sequences:
##
## .. code::
##
##    SUCCESS
##    MIMMMMM
##    A-CCESS
##
## If you want to know more about Cigar strings, go read `the SAM spec <http://samtools.github.io/hts-specs/SAMv1.pdf>`_.

import std / [sequtils]

const LibName = "libedlib.so"

{.push callConv: cdecl, dynlib: LibName.}

type
  Status* = enum
    OK, ERROR

  Mode* = enum
    ## Alignment methods:
    ##
    ## `NW` or Needleman-Wunsch, the global alignment and standard.
    ##
    ## `SHW` or Prefix, doesn't penalize gaps at query end. Use this to
    ## find out how well the first sequence fits at the begining of the
    ## second sequence.
    ##
    ## `HW` or Infix, similar to SHW but gaps at query end or start are
    ## not penalized. Use this to find where a small sequence fits in a
    ## longer sequence.
    NW, SHW, HW

  Task* = enum
    ## What task to perform:
    ##
    ## `DISTANCE` finds edit distance and end locations.
    ##
    ## `LOC` finds also the start locations.
    ##
    ## `PATH` finds also the alignment path or cigar.
    DISTANCE, LOC, PATH

  CigarFormat* = enum
    ## Cigar format, as seen at:
    ##
    ##  http://samtools.github.io/hts-specs/SAMv1.pdf
    ##
    ##  http://drive5.com/usearch/manual/cigar.html
    STANDARD, EXTENDED

  Edop* = enum
    MATCH, INSERT, DELETE, MISMATCH

  EqualityPair* {.bycopy.} = object
    first*, second*: char

  AlignConfig* {.bycopy.} = object
    ## The configuration to pass to the
    ## `alignment<#align,cstring,cint,cstring,cint,AlignConfig>`_ procedure
    ## config parameter.
    ##
    ## `k` tells edlib that edit distance is not larger than `k`. Smaller `k`
    ## is faster. If `k` is set to a negative value, edlib will auto-adjust.
    k*: cint
    mode*: Mode
    task*: Task
    additionalEqualities*: ptr EqualityPair
    additionalEqualitiesLength*: cint

  AlignResult* {.bycopy.} = object
    ## Stores the result of the
    ## `alignment<#align,cstring,cint,cstring,cint,AlignConfig>`_.
    ##
    ## `status` matches the `Status<#Status>`_ enum value
    ##
    ## `editDistance` is -1 if *k* is non-negative and edit distance is larger
    ## than *k*.
    ##
    ## `endLocations` and `startLocations` should be manually free if you don't
    ## use `freeAlignResult<#freeAlignResult,AlignResult>`_.
    ##
    ## `numLocations` is the number of *endLocations* and *startLocations*.
    ##
    ## `alignment` is a sequence of `CigarOps<#CigarOp>`_.
    status*: cint
    editDistance*: cint
    endLocations*: ptr cint
    startLocations*: ptr cint
    numLocations*: cint
    alignment*: ptr cchar
    alignmentLength*: cint
    alphabetLength*: cint

proc newAlignConfig*(k: cint; mode: Mode; task: Task;
                     additionalEqualities: ptr EqualityPair;
                     additionalEqualitiesLength: cint): AlignConfig {.
    importc: "edlibNewAlignConfig".}
    ## Return an `AlignConfig<#AlignConfig>`_ to use as parameter to
    ## `align<#align,cstring,cint,cstring,cint,AlignConfig>`_.
    ##
    ## Usually you'll want to change the `Task<Task>`_ to find the starting
    ## locations or the alignment path, or the `Mode<Mode>`_ to tune the way
    ## the query is matched against the target.
    ##
    ## `additionalEqualities` stores the pairs of characters that should be
    ## considered equal, e.g. consider "A" and "a" the same.

proc defaultAlignConfig*(): AlignConfig {.
    importc: "edlibDefaultAlignConfig".}
    ## Returns a default `AlignConfig<#AlignConfig>`_ with:
    ##
    ## `k` = -1
    ##
    ## `mode` = `Mode<#Mode>`_.NW
    ##
    ## `task` = `Task<#Task>`_.DISTANCE
    ##
    ## `additionalEqualities` = nil

proc freeAlignResult*(result: AlignResult) {.
    importc: "edlibFreeAlignResult".}
    ## Frees the `AlignResult<#AlignResult>`_ memory.

proc align*(query: cstring; queryLength: cint; target: cstring;
            targetLength: cint; config: AlignConfig): AlignResult {.
    importc: "edlibAlign".}
    ## Performs an alignment of the query against the target.
    ##
    ## `config` fine-tunes the `Task<#Task>`_ and `Mode<#Mode>`_ of the
    ## alignment.

proc alignmentToCigar*(alignment: ptr cchar; alignmentLength: cint;
                       cigarFormat: CigarFormat): cstring {.
    importc: "edlibAlignmentToCigar".}
    ## Builds the Cigar string for an `alignment<#AlignResult>`_ sequence.
{.pop.}

proc alignmentToCigar*(align: AlignResult,
                       cigarFormat: CigarFormat=CigarFormat.STANDARD): string =
  ## Convenience method to call the edlib native alignmentToCigar with Nim
  ## types and without the need to pass the alignment pointer & length.
  ##
  # Explicit conversion, happy compiler
  var cigar = alignmentToCigar(align.alignment, align.alignmentLength,
                               cigarFormat)
  $cigar

proc alignmentToEdops*(align: AlignResult): seq[Edop] =
  ## Return a seq of `Edops<#Edop>`_ from an `AlignResult<#AlignResult>`_.
  var ops = cast[ptr UncheckedArray[Edop]](align.alignment)
  toSeq(toOpenArray(ops, 0, align.alignmentLength - 1))

proc align*(query, subject: string, config: AlignConfig): AlignResult =
  ## Convenience method to call the edlib native align with Nim types and
  ## without the need to pass the string lengths.
  align(
    cstring(query), cint(query.len), cstring(subject), cint(subject.len),
    config)
