# A wrapper to edlib

This is a c2nim wrapper to edlib library, modified to make it to my likes and
to use it from Bio.

It was created against edlib v1.2.7.

# Install edlib C library

Follow the instructions at https://github.com/Martinsos/edlib to install the
shared lib in your machine. Assuming you already have Nim correctly installed:

  - Get Meson (https://mesonbuild.com/) installed.

  - Clone the repo with $ git clone https://github.com/Martinsos/edlib.git                            
  - Enter the `edlib` directory and execute `make LIBRARY_TYPE=shared`. This
    should create a `meson-build` directory, with at least `libedlib.so.1` and
    `libedlib.so`.

  - Install those files to a path where the Nim compiler can found them:

        $ sudo make install

    This should tell you where the files are being installed, with something
    like `Installing libedlib.so.1 to /usr/local/lib64`. Make sure that path
    is on the scope, or you need to put it in `LD_LIBRARY_PATH` environment.

# Install this wrapper

    $ nimble install nedlib

# Check everything went fine

  Create a `test.nim` file with the contents:

  ```
  import nedlib

  let cfg = defaultAlignConfig()
  echo align("SUCCESS", "SUCESS", cfg)
  ```

  Compile-run it with `nim r test.nim`.

  **FINE OUTPUT**

    (status: 0, editDistance: 1, endLocations: ..., startLocations: ..., numLocations: 1, alignment: nil, alignmentLength: 0, alphabetLength: 4)

  **KNOWN PROBLEMS**

  `could not load: libedlib.so` means Nim cannot find the file. Either it
  wasn't installed or it isn't in your `LD_LIBRARY_PATH`

# Other wrappers to edlib

[Nim-edlib](https://github.com/bio-nim/nim-edlib) is a plain c2nim wrapper.

[nimedlib](https://github.com/sstadick/nimedlib) is a nimteropt wrapper. I
couldn't make it work, nor make a new nimteropt builder.
